/* Author : Prashinka Sahu
 * Date : 26 Feb 2018
 * Purpose : This class contains all the required elements of Flight Search Page on 
 * Orbitz Application.
 * 
 */

package orbitz.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Flights_Search {
	By flying_From = By.cssSelector("#flight-origin");
	By flying_To = By.cssSelector("#flight-destination");
	By oneWay_Trip = By.cssSelector("#flight-type-one-way-label");
	By roundTrip = By.cssSelector("#flight-type-roundtrip-label");
	By search_Button = By.cssSelector("#search-button");
	By result_Title = By.cssSelector(".title-city-text");

	public WebElement create_Flying_From(WebDriver driver) {
		return (new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(flying_From)));
	}

	public WebElement create_Flying_To(WebDriver driver) {
		return (new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(flying_To)));
	}

	public WebElement create_OneWay_Trip(WebDriver driver) {
		return (new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(oneWay_Trip)));
	}

	public WebElement create_RoundTrip(WebDriver driver) {
		return (new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(roundTrip)));
	}

	public WebElement create_Search_Button(WebDriver driver) {
		return (new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(search_Button)));
	}

	public WebElement create_Result_Title(WebDriver driver) {
		return (new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(result_Title)));
	}

}
