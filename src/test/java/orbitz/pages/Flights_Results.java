/* Author : Prashinka Sahu
 * Date : 26 Feb 2018
 * Purpose : This class contains all the required elements of Home Page on 
 * Orbitz Application.
 * 
 */

package orbitz.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Flights_Results {

	By result_title = By.cssSelector(".title-city-text");
	By result_List_CityCode = By.cssSelector(".secondary-content.no-wrap");
	By destination_CityCode = By.cssSelector("#departure-airport-1");
	By source_CityCode = By.cssSelector("#arrival-airport-1");

	public WebElement create_result_Title(WebDriver driver) {
		return (new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(result_title)));
	}

	public List<WebElement> create_Result_List(WebDriver driver) {
		return (new WebDriverWait(driver, 30)
				.until(ExpectedConditions.presenceOfAllElementsLocatedBy(result_List_CityCode)));
	}

	public WebElement create_Source_CityCode(WebDriver driver) {
		return (new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(source_CityCode)));
	}

	public WebElement create_Destination_CityCode(WebDriver driver) {
		return (new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(destination_CityCode)));
	}

}
