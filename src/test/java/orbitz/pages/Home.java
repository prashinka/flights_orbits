/* Author : Prashinka Sahu
 * Date : 26 Feb 2018
 * Purpose : This class contains all the required elements of Home Page on 
 * Orbitz Application.
 * 
 */

package orbitz.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Home {

	By Flights_Only = By.id("primary-header-flight");

	public WebElement create_Flights_Only(WebDriver driver) {
		return (new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(Flights_Only)));
	}
}
